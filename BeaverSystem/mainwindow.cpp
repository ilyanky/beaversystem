#include "mainwindow.h"
#include "DisplacementMethod.h"
#include <QMenu>
#include <QMenuBar>
#include <QQuickView>
#include <QWidget>
#include <QDebug>
#include <QObject>
#include <QVariant>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickItem>

BeaverSystem::BeaverSystem(QWidget *parent)
    : QWidget(parent)
{

//    QQuickView* view = new QQuickView; //---нужно ли удалять вручную?
    view.setSource(QUrl::fromLocalFile("../../BeaverSystem/QMLWidget/MainWindow.qml"));
//    QWidget* central = QWidget::createWindowContainer(&view, this); //---нужно ли удалять вручную?
//    this->resize(view.width(), view.height());
    view.show();
}


BeaverSystem::~BeaverSystem()
{

}


std::pair<int, QList<float*> > BeaverSystem::getInfo()
{
    QQuickItem* root = view.rootObject();
    QVariant var = root->property("barArr");
    QList<QVariant> list = var.toList();

    std::pair<int, QList<float*> > info;
    int size = list.size();
    info.first = size;
    float* vec_L = new float[size];
    float* vec_E = new float[size];
    float* vec_A = new float[size];
    float* d_forces = new float[size];
    for ( int i = 0; i < list.size(); ++i ) {
        QObject* obj = qvariant_cast<QObject*>(list.at(i));
        vec_L[i] = obj->property("coef_L").toFloat();
        vec_E[i] = obj->property("coef_E").toFloat();
        vec_A[i] = obj->property("coef_A").toFloat();
        d_forces[i] = obj->property("d_forceCoef").toFloat();
    }
    info.second.push_back(vec_L);
    info.second.push_back(vec_E);
    info.second.push_back(vec_A);
    info.second.push_back(d_forces);

    qDebug() << *info.second.at(0);
    qDebug() << *info.second.at(1);
    qDebug() << *info.second.at(2);
    qDebug() << *info.second.at(3);

//    var = root->property("restrArr");
//    list = var.toList();
//    float* vec_Restr = new float[size+1];
//    for ( int i = 0; i < list.size(); ++i ) {
//        QObject* obj = qvariant_cast<QObject*>(list.at(i));
//        vec_Restr[i] = obj->property("isThere").toFloat();
//    }
//    info.second.push_back(vec_Restr);

    var = root->property("conctrForceArr");
    list = var.toList();
    float* c_forces = new float[size+1];
    for ( int i = 0; i < list.size(); ++i )
        c_forces[i] = list.at(i).toFloat();
    info.second.push_back(c_forces);

    return info;
}


void BeaverSystem::temp()
{
    getStrengthCalcs(getInfo());
}
