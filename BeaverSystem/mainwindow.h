#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QQuickView>
#include <QWidget>

class BeaverSystem : public QWidget
{
    Q_OBJECT

private:
    QQuickView view;

public:
    BeaverSystem(QWidget *parent = 0);
    ~BeaverSystem();
    std::pair<int, QList<float*> > getInfo();

public slots:
    void temp();
};

#endif // MAINWINDOW_H
