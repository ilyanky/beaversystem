#ifndef DISPLACEMENTMETHOD_H
#define DISPLACEMENTMETHOD_H

#include <QList>
#include <utility>

double* getMatrix_A(const double* const vec_L, const double* const vec_E,
                   const double* const vec_A, const double* vec_restr,
                   const int& len);

double* getVector_b(const double* const d_forces, const double* const c_forces,
                   const double* const vecL, const int& len);

double* getDeltas(const double* const matrix_A, const double* const vec_b,
                 const int& len);

QList<QList<double> > getStrengthCalcs(const std::pair<int, QList<double*> > info);

#endif // DISPLACEMENTMETHOD_H
