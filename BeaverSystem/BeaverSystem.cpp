#include "BeaverSystem.h"
#include "DisplacementMethod.h"
#include <QVariant>
#include <QDebug>
#include <QFile>
#include <QDataStream>

BeaverSystem::BeaverSystem(QObject *parent) :
    QObject(parent)
{
    m_engine.rootContext()->setContextProperty("some", this);
    //m_engine.globalObject().setProperty("someValue", QJSValue(12));
}


void BeaverSystem::load()
{
    m_engine.load(QUrl(QStringLiteral("qrc:///content/MainWindow.qml")));
}


QJSValue BeaverSystem::startCalc(const QJSValue& barArr, const QJSValue& forces)
{
//    QVariant var = barArr.toVariant().toList();
    QList<QVariant> list = barArr.toVariant().toList();

    std::pair<int, QList<double*> > info;
    int size = list.size();
    info.first = size;
    double* vec_L = new double[size];
    double* vec_E = new double[size];
    double* vec_A = new double[size];
    double* d_forces = new double[size];
    double* vec_restr = new double[size+1];
    for ( int i = 0; i < list.size(); ++i ) {
        QObject* obj = qvariant_cast<QObject*>(list.at(i));
        vec_L[i] = obj->property("coef_L").toDouble();
        vec_E[i] = obj->property("coef_E").toDouble();
        vec_A[i] = obj->property("coef_A").toDouble();
        d_forces[i] = obj->property("d_forceCoef").toDouble();
        vec_restr[i] = obj->property("beginRestrVisible").toDouble();
        if ( i == list.size() - 1 )
            vec_restr[i+1] = obj->property("endRestrVisible").toDouble();
    }
    info.second.push_back(vec_L);
    info.second.push_back(vec_E);
    info.second.push_back(vec_A);
    info.second.push_back(d_forces);
    info.second.push_back(vec_restr);

    qDebug() << *info.second.at(0);
    qDebug() << *info.second.at(1);
    qDebug() << *info.second.at(2);
    qDebug() << *info.second.at(3);
    qDebug() << *info.second.at(4);
    qDebug() << *(info.second.at(4) + 1);

    //вот где-то здесь ошибка, связанная с силами
    list = forces.toVariant().toList();
    double* c_forces = new double[list.size()];
    for ( int i = 0; i < list.size(); ++i )
        c_forces[i] = list.at(i).toDouble();
    info.second.push_back(c_forces);

    qDebug() << *info.second.at(5);

    QList<QList<double> > res = getStrengthCalcs(info);
    delete[] vec_L;
    delete[] vec_E;
    delete[] vec_A;
    delete[] d_forces;
    delete[] c_forces;
    delete[] vec_restr;

    QJSValue arr = m_engine.newArray(res.size());
    for ( int i = 0; i < res.size(); ++i ) {
        QJSValue tempArr = m_engine.newArray(res.at(i).size());
        for ( int j = 0; j < res.at(i).size(); ++j )
            tempArr.setProperty(j, res.at(i).at(j));
        arr.setProperty(i, tempArr);
    }

    return arr;
}


bool BeaverSystem::saveAs(const QUrl& url, const QJSValue& barArr,
                          const QJSValue& forces)
{
    QFile file(url.toLocalFile());
    if ( !file.open(QIODevice::WriteOnly) )
        return false;
    QDataStream fileStream(&file);
    fileStream << MAGIC;

    QList<QVariant> list = barArr.toVariant().toList();
    QList<double> vecL;
    QList<double> vecE;
    QList<double> vecA;
    QList<double> dForces;
    QList<double> vecRestr;
    QList<double> vec_vsblConctr;
    for ( int i = 0; i < list.size(); ++i ) {
        QObject* obj = qvariant_cast<QObject*>(list.at(i));
        vecL << obj->property("coef_L").toDouble();
        vecE << obj->property("coef_E").toDouble();
        vecA << obj->property("coef_A").toDouble();
        dForces << obj->property("d_forceCoef").toDouble();
        vecRestr << obj->property("beginRestrVisible").toDouble();
        vec_vsblConctr << obj->property("beginConctrVisible").toDouble();
        vec_vsblConctr << obj->property("endConctrVisible").toDouble();
        if ( i == list.size() - 1 )
            vecRestr << obj->property("endRestrVisible").toDouble();
    }

    QList<double> cForces;
    list = forces.toVariant().toList();
    for ( int i = 0; i < list.size(); ++i )
        cForces << list.at(i).toDouble();

    fileStream << vecL;
    fileStream << vecE;
    fileStream << vecA;
    fileStream << dForces;
    fileStream << vecRestr;
    fileStream << cForces;
    fileStream << vec_vsblConctr;

    if ( fileStream.status() != QDataStream::Ok )
        return false;

    return true;
}


QJSValue BeaverSystem::open(const QUrl& url)
{
    QJSValue result = m_engine.newObject();
    QFile file(url.toLocalFile());
    if ( !file.open(QIODevice::ReadOnly) ) {
        result.setProperty("error", QJSValue("Can't open a file"));
        return result;
    }
    QDataStream fileStream(&file);
    int magic;
    fileStream >> magic;
    if ( magic != MAGIC ) {
        result.setProperty("error", QJSValue("File is corrupt"));
        return result;
    }

    QList<double> info;
    QList<QString> params;
    params << "vec_L" << "vec_E" << "vec_A" << "d_Forces"
           << "vec_Restr" << "c_Forces" << "vec_vsblConctr";
    for ( int i = 0; i < params.size(); ++i ) {
        fileStream >> info;
        QJSValue temp = m_engine.newArray(info.size());
        for ( int j = 0; j < info.size(); ++j )
            temp.setProperty(j, QJSValue(info.at(j)));
        result.setProperty(params.at(i), temp);
    }
//    fileStream >> info;
//    QJSValue temp = m_engine.newArray(info.size());
//    for ( int j = 0; j < info.size(); ++j )
//        temp.setProperty(j, QJSValue(info.at(j)));
//    result.setProperty("c_Forces", temp);

    if ( fileStream.status() != QDataStream::Ok ) {
        result.setProperty("error", QJSValue("Some errros occured while reading a file."));
        return result;
    }

    qDebug() << "there2";
    result.setProperty("error", QJSValue("no errors"));
    return result;
}

