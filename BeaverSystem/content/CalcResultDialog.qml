import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "logic.js" as Logic

Dialog {
    id: dialog
    title: "Calculations result"
    modality: Qt.ApplicationModal
//    standardButtons: StandardButton.Ok
    width: 768
    height: 668
    property alias tableModel: tableMdl

    function updateInfo(Nx_1, Nx_2, gx) {
        var info = {
            restrCount: 0,
            conctrCount: 0,
            distrCount: 0,
            barCount: 0,
            nodeCount: 0,
            vec_L: [],
            vec_Nx_1: [],
            vec_Nx_2: [],
            vec_gx: []
        }
        for ( var i = 0; i < Logic.barPartArr.length; ++i ) {
            if ( Logic.barPartArr[i].d_forceCoef != 0 )
                ++info.distrCount;
            if ( Logic.barPartArr[i].beginRestrVisible )
                ++info.restrCount;
            if ( i == Logic.barPartArr.length - 1 )
                if ( Logic.barPartArr[i].endRestrVisible )
                    ++info.restrCount;
            info.vec_L[i] = Logic.barPartArr[i].coef_L;
        }
        info.barCount = Logic.barPartArr.length;
        info.nodeCount = Logic.barPartArr.length + 1;
        for ( var i = 0; i < Logic.c_forces.length; ++i )
            if ( Logic.c_forces[i] != 0 )
                ++info.conctrCount;

        info.vec_Nx_1 = Nx_1;
        info.vec_Nx_2 = Nx_2;
        info.vec_gx = gx;

        barsTxt.text = "Bars number: " + info.barCount;
        nodesTxt.text = "Nodes number: " + info.nodeCount;
        forcesTxt.text = "Forces number: " + (info.distrCount +
                info.conctrCount) + "<br>Distriction force: " +
                info.distrCount + "<br>Concentrated force: " +
                info.conctrCount;
        restrTxt.text = "Restrictions number: " + info.restrCount;

        tableMdl.clear();
        for ( var i = 0; i < Logic.barPartArr.length; ++i )
            tableMdl.append({"barNum": i,
                                "minNx": info.vec_Nx_1[i],
                                "maxNx": info.vec_Nx_1[i] +
                                    info.vec_Nx_2[i] * info.vec_L[i],
                                "gx": info.vec_gx[i],
                                "crash": (info.vec_gx[i] > Logic.gx) ? "+" : "-"
                            });

        canvas.segCount = info.barCount;
        canvas.vecNx = [info.vec_Nx_1, info.vec_Nx_2];
        canvas.vecL = info.vec_L;
        canvas.requestPaint();
    }

    contentItem: Column {
//        implicitHeight: 500
//        implicitWidth: 500
        spacing: 10
        anchors {
            fill: parent
            margins: 20
        }

        GroupBox {
            id: grp
            title: "Construction info"
            anchors.left: parent.left
            anchors.right: parent.right
//            anchors.top: parent.top
            height: 150
            GridLayout {
                columns: 2
                flow: GridLayout.TopToBottom
                Text { id: barsTxt; text: "Bars number: " }
                Text { id: nodesTxt; text: "Nodes number: " }
                Text { id: forcesTxt; text: "Forces number: " }
                Text { id: restrTxt; text: "Restrictions number: " }
            }
        }
//        TableView {
//            id: tbl
//            anchors.left: parent.left
//            anchors.right: parent.right
//            anchors.top: parent.top
//            TableViewColumn { role: "barNum"; title: "Bar №" }
//            TableViewColumn { role: "Nx_1"; title: "Nx_1" }
//            TableViewColumn { role: "Nx_2"; title: "Nx_2" }
//        }

        ScrollView {
            id: scroll
            frameVisible: true
            anchors {
                left: parent.left
                right: parent.right
//                top: grp.bottom
//                bottom: tab.top
            }
            height: 450

            Canvas {
                property int segCount
                property var vecNx
                property var vecL

                function drawEpure(segmentCount, vec_Nx, vec_L)
                {
                    canvas.requestPaint();
                    var ctx = canvas.getContext("2d");
                    ctx.clearRect(0, 0, 800, 800);

                    ctx.beginPath();
                    ctx.lineWidth = 3;
                    ctx.strokeStyle = "black";
                    ctx.moveTo(40, 10);
                    ctx.lineTo(40, 800);
                    ctx.moveTo(40, 390);
                    ctx.lineTo(800, 390);
                    ctx.stroke();
                    ctx.closePath();

                    var prevX = 40;
                    for ( var i = 0; i < segmentCount; ++i ) {
                        if ( i == 0 ) {
                            ctx.beginPath();
                            ctx.lineWidth = 1;
                            ctx.font = "15px serif"
                            ctx.strokeStyle = "black";
                            var num = vec_Nx[0][i] + vec_Nx[1][i] * 0;
                            ctx.strokeText(num + "qL", prevX - 35,
                                           390 - (vec_Nx[0][i] + vec_Nx[1][i] * 0) * 10);
                            ctx.closePath();
                        }

                        ctx.beginPath();
                        ctx.strokeStyle = "red";
                        ctx.lineWidth = 3;
                        ctx.moveTo(prevX, 390 - (vec_Nx[0][i] + vec_Nx[1][i] * 0) * 10);
                        ctx.lineTo(prevX + vec_L[i] * 100,
                                   390 - (vec_Nx[0][i] + vec_Nx[1][i] * vec_L[i]) * 10);
                        prevX = prevX + vec_L[i] * 100;
                        ctx.stroke();
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.lineWidth = 1;
                        ctx.font = "15px serif"
                        ctx.strokeStyle = "black";
                        var num = vec_Nx[0][i] + vec_Nx[1][i] * vec_L[i];
                        ctx.strokeText(num + "qL", prevX,
                                       390 - (vec_Nx[0][i] + vec_Nx[1][i] * vec_L[i]) * 10 + 10);
                        ctx.closePath();

                        ctx.beginPath();
                        ctx.lineWidth = 1;
                        ctx.strokeStyle = "black";
                        ctx.moveTo(prevX, 10);
                        ctx.lineTo(prevX, 800);
                        ctx.stroke();
                        ctx.closePath();
                    }
                }

                id: canvas
                width: 800
                height: 800
                onPaint: {
//                    var ctx = canvas.getContext("2d");
//                    ctx.clearRect(0, 0, 800, 800);
//                    console.log(constrInfo.barCount, constrInfo.vec_Nx_1, constrInfo.vec_Nx_2, constrInfo.vec_L);
                    drawEpure(segCount, vecNx, vecL);
                }
            }
        }


        TableView {
            id: tab
//            anchors.top: scroll.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 150
            //anchors.bottom: parent.bottom
            TableViewColumn { role: "barNum"; title: "Bar №" }
            TableViewColumn { role: "minNx"; title: "Min Nx" }
            TableViewColumn { role: "maxNx"; title: "Max Nx" }
            TableViewColumn { role: "gx"; title: "\u03C3" }
            TableViewColumn { role: "crash"; title: "All crashs?" }
            model: ListModel {
                id: tableMdl
            }
        }

        Button {
            text: "OK"
            onClicked: dialog.close();
//            height: 50
//            implicitHeight: 50
            anchors {
//                top: tab.bottom
                right: parent.right
            }
        }

    }

}
