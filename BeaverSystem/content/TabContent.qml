import QtQuick 2.2
import QtQuick.Controls 1.2
//import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
//import QtQuick.Dialogs 1.2
//import "logic.js" as Logic
Tab {
    property var barInfo

//    function setText(info)
//    {
//        nx_1.text = "First coef Nx: " + info.vec_Nx_1;
//        nx_2.text = "Second coef Nx: " + info.vec_Nx_2;
//        gx.text = "gx: " + info.vec_gx;
//    }

    Item {

        anchors.margins: 5

        GridLayout {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            columns: 2
            flow: GridLayout.TopToBottom
            Text { text: "First coef Nx: " + barInfo.vec_Nx_1 }
            Text { text: "Second coef Nx: " + barInfo.vec_Nx_2 }
            Text { text: "gx: " + barInfo.vec_gx }
            //            Text { text: "Force number: " + barInfo.forceCount }
            //            Text { text: "Restriction number: " + barInfo.restrCount }
        }
    }
}
