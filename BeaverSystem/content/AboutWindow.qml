import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2

Dialog
{
    id: dialog
    width: 750
    height: 300
    modality: Qt.NonModal
    title: "BeaverDam About"

    contentItem: Rectangle {
        anchors.fill: parent
        color: "whitesmoke"

        Image {
            id: img
            source: "resources/stolenLogo.png"
            anchors.left: parent.left
            anchors.top: parent.top
            sourceSize: Qt.size(200, 200)
        }
        Column {
            anchors.left: img.right
            anchors.right: parent.right
            spacing: 15
            Text {
                wrapMode: Text.WordWrap
                width: 500
                font.pointSize: 12
                text: "This is a student project, which purpose is the creation of an automation system of strength calculations."
            }
            Text {
                wrapMode: Text.WordWrap
                width: 500
                font.pointSize: 12
                text: "Full technical task of the project here...:"
                font.underline: true
                color: mouseArea.containsMouse ? "blue" : "balck"
                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: scrollView.visible ? scrollView.visible = false :
                                                    scrollView.visible = true;
                }
            }
            ScrollView {
                id: scrollView
                anchors.left: parent.left
                anchors.right: parent.right
                height: 100
                visible: false
                Text {
                    width: 500
                    textFormat: Text.StyledText
                    wrapMode: Text.WordWrap
                    text: "<b>Техническое задание на разработку
                       системы автоматизации прочностных расчетов стержневых систем,
                    испытывающих растяжение-сжатие</b><br>
                    <br><b>1. Требования к конструкции</b><br>
                    <br>Конструкция должна представлять собой плоскую стержневую систему,
                    составленную из прямолинейных стержней, последовательно соединенных друг
                    с другом вдоль общей оси.
                    Каждый стержень  характеризуются длиной , площадью поперечного сечения .
                    Материал стержней должен характеризоваться модулем упругости , допускаемым
                    напряжением .
                    <br><br><b>2. Требования к нагрузкам</b><br>
                    На любое сечение конструкции могут быть наложены нулевые кинематические
                    граничные условия (жесткие опоры), запрещающие перемещения и повороты этих сечений
                    во всех направлениях.
                    Конструкция может быть нагружена в глобальных узлах  статическими сосредоточенными
                    продольными усилиями .
                    Каждый стержень конструкции может быть нагружен постоянной вдоль его оси статической
                    погонной нагрузкой .
                    <br><br><b>3. Требования к задачам</b><br>
                    Система должна обеспечивать решение линейной задачи статики для плоских стержневых
                    конструкций.
                    <br><br><b>4. Общесистемные требования</b>
                    Система должна работать на персональных компьютерах, работающих под управлением
                    операционной системы Windows XP.
                    <br><br><b>5. Требования к системе</b>
                    <br><i>5.1. Требования к препроцессору</i>
                    <br>Препроцессор системы должен обеспечивать:
                    ввод массивов данных, описывающих конструкцию и внешние воздействия;
                    формальную диагностику данных, описывающих конструкцию и внешние воздействия;
                    визуализацию конструкции и нагрузок.
                    <br><i>5.2. Требования к процессору</i>
                    <br>Процессор системы должен обеспечивать расчет компонент напряженно-деформированного
                     состояния конструкции.
                    <br><i>5.3. Требования к постпроцессору</i>
                    <br>Постпроцессор системы должен обеспечивать:
                    формирование файла результатов расчета;
                    отображение результатов расчета в табличном виде;
                    отображение результатов расчета в виде графиков, на оси ординат которых отложены
                    интересующие пользователя компоненты напряженно-деформированного состояния
                    конструкции, а на оси абсцисс –
                    локальные координаты стержней;
                    <br>* отображение результатов расчета в виде эпюр компонент
                    напряженно-деформированного
                    состояния на конструкции.
                <br><br>* – необязательное требование"
                }
            }
            Text {
                wrapMode: Text.WordWrap
                width: 500
                font.pointSize: 12
                textFormat: Text.StyledText
                text: "This project was created with Qt 5.3.1 and Qt Creator 3.1.2.
                   Whole interface is written in QML and JS, but all calculations in C++."
            }
            Text {
                wrapMode: Text.WordWrap
                width: 500
                font.pointSize: 12
                textFormat: Text.StyledText
                text: "MSTU STANKIN 2014"
            }
        }
        Text {
            wrapMode: Text.WordWrap
            width: 500
            font.pointSize: 8
            textFormat: Text.StyledText
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.topMargin: 10
            //text: "Performed by Ilya Kuznetsov.."
            text: mouseArea2.containsMouse ? "Выполнил студент 3 курса, группы ИДБ-12-03 Кузнецов Илья"
                                           : "Performed by Ilya Kuznetsov.."
            MouseArea {
                id: mouseArea2
                hoverEnabled: true
                anchors.fill: parent
            }
        }
        Button {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            text: "Close"
            onClicked: dialog.close();
        }
    }
}

