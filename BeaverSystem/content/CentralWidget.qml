import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import "logic.js" as Logic

Item
{
    readonly property alias coordArrowX: coordSystem.coordArrowX

    id: widget

    onHeightChanged: {
        if ( coordSystem.height < height )
            coordSystem.y = height / 2 - coordSystem.height / 2;
    }

    /********** Canvas for drawing a bars **********/
    ScrollView {
        id: scroll
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: toolBar.right

        contentItem: CoordinateSystem {
            id: coordSystem
            width: 400
            height: 400
        }
    }

    /********** Tool Bar **********/
    Rectangle
    {
        id: toolBar
        width: 50
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        color: "#b2aeb5"

        Column {
            anchors.fill: parent
            anchors.topMargin: 5
            spacing: 5

            //STATES!!!!!!
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/rect.png"
                tooltip: "Add a bar part"
                onClicked: {
                    if ( window.toolBar )
                        window.toolBar.visible = false;
                    window.toolBar = addBarTool;
                    window.toolBar.visible = true;
                }

                AddBarTool {
                    id: addBarTool
                }
            }
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/remove.svg"
                tooltip: "Delete last part"
                onClicked: Logic.removeBar();
            }
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/bigRestr30.svg"
                tooltip: "Add a restrict"
                onClicked: {
                    if ( window.toolBar )
                        window.toolBar.visible = false;
                    window.toolBar = addRestrTool;
                    window.toolBar.visible = true;
                }

                AddRestrTool {
                    id: addRestrTool
                }
            }
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/distrForce30.svg"
                tooltip: "Add a distributed force"
                onClicked: {
                    if ( window.toolBar )
                        window.toolBar.visible = false;
                    window.toolBar = addDistrTool;
                    window.toolBar.visible = true;
                }

                AddDistrTool {
                    id: addDistrTool
                }
            }
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/conctrForce30.svg"
                tooltip: "Add a concentrated force"
                onClicked: {
                    if ( window.toolBar )
                        window.toolBar.visible = false;
                    window.toolBar = addConctrTool;
                    window.toolBar.visible = true;
                }

                AddConctrTool {
                    id: addConctrTool
                }
            }
            ToolButton {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/magnifier.png"
                tooltip: "Scale bars"
                onClicked: {
                    if ( window.toolBar )
                        window.toolBar.visible = false;
                    window.toolBar = scaleTool;
                    window.toolBar.visible = true;
                }

                ScaleTool {
                    id: scaleTool
                }
            }
        }


        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 100
            Text {
                id: txt
                anchors.top: parent.top
                //verticalAlignment: Text.AlignVCenter
                text: "Results:"
            }
            ToolButton {
                id: play
                anchors.top: txt.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/play30.png"
                tooltip: "Start calculations"
                onClicked: {
                    setGx.open();
                    //window.startCalculations();
                }
            }
            ToolButton {
                anchors.top: play.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 30
                iconSource: "resources/eye30.png"
                tooltip: "Show results"
                onClicked: window.showResults();
            }

            MiniDialog {
                id: setGx
                text: "Set max of gx parametr: "
                onAccepted: {
                    Logic.gx = parseFloat(newValue);
                    window.startCalculations();
                }
            }
        }
    }

}
