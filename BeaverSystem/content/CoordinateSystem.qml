import QtQuick 2.2

Rectangle {
    property alias coordArrowX: coordinateArrowX
    property bool run: false
    color: "transparent"
//    y: mouseArea.mouseY
    id: area

    onHeightChanged: {
        console.log(height / 2, " --- ", coordinateArrowX.y);
        //y += height / 2
    }



    Rectangle {

        property real idontknow: childrenRect.x /* because I DON'T KNOW, but without any appeal to childRect, signal
                                                        childrenRectChanged does not emitted */
        id: coordinateArrowX
        width: parent.width
//        y: parent.height / 2
        anchors.verticalCenter: parent.verticalCenter
        height: 3
        color: "black"

        onChildrenRectChanged: {
            var num = -(childrenRect.width + childrenRect.x - parent.width);
            if ( num < 100 ) {
                if ( num < 0 )
                    parent.width += (-num + 100);
                else
                    parent.width += (100 - num);
            }
            if ( childrenRect.height + 100 > parent.height ) { //поставить условие, как сверху
                var num = (childrenRect.height + 100 - parent.height);
                parent.height += num;
                if ( parent.height > widget.height )
                    parent.y -= parent.y;
                else
                    parent.y = widget.height / 2 - parent.height / 2;
            }
        }

    }

    Rectangle {
        id: coordinateArrowY
        height: parent.height
        width: 3
        x: 100
        color: "black"

        Image {
            width: 30
            height: 30
            source: "./resources/coordArrowY2.svg"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                cursorShape: Qt.SizeVerCursor
                property real a

                onPressed: a = mouse.y;
                onClicked: {
                    if ( area.y > 0 )
                        area.y -= 20;
                    area.height += 40;
                }
                onPositionChanged: {
                    if ( a > mouse.y ) {
                        if ( area.y > 0 )
                            area.y -= 2;
                        area.height += 4;
                    }
                    else if ( a < mouse.y )
                        if ( area.height > 400 ) {
                            area.y += 2;
                            area.height -= 4;
                        }
                }
            }
        }
    }

    Image {
        width: 30
        height: 30
        source: "./resources/coordArrowX2.svg"
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.SizeHorCursor
            property real a

            onPressed: a = mouse.x;
            onPositionChanged: {
                if ( a < mouse.x )
                    area.width += 10;
                else if ( a > mouse.x )
                    if ( area.width > coordinateArrowX.childrenRect.width + 200 )
                        area.width -= 10;
            }
        }
    }

}
