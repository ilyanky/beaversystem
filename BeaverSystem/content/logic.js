//создать позже конструктор конструкции - пусть отражает класс Конструкции
.pragma library // Shared state
.import QtQuick 2.2 as QQ

var barPartComponent = null;
var tabComponent = null;

var beginX = 100;
var scaleValue = 1;
var barPartArr = [];
var c_forces = [];
var constrChanged = true;
var gx = 0;

function createBarObject(parent, u_l, u_a, u_e, u_x, u_width, num)
{
    if ( barPartComponent.status == QQ.Component.Ready ) {
        if ( u_x === undefined ) {
            var x = 0;
            if ( barPartArr.length === 0 )
                x = beginX;
            else {
                var lastObj = barPartArr[barPartArr.length - 1];
                x = lastObj.x + lastObj.width;
            }
        }
        else
            x = u_x;

        var object = barPartComponent.createObject(parent, {"x": x,
                                                  "anchors.verticalCenter": parent.verticalCenter,
                                                  "eaText": "1E, " + u_a + "A",
                                                  "lText": u_l + "L",
                                                  "coef_A": u_a,
                                                  "coef_L": u_l,
                                                  "coef_E": u_e
                                              });
        if ( u_width === undefined ) {
            object.width *= paramCoef(u_l) * scaleValue;
            object.height *= paramCoef(u_a) * scaleValue;
        }
        else {
            object.width = u_width;
            object.height *= scaleValue;
        }

        //    if ( x + object.width > parent.width )
        //        parent.width += (x + object.width + 20 - parent.width);

        if ( num == undefined )
            barPartArr.push(object);
        else
            barPartArr.splice(num, 0, object);
        enumBars();

        if ( barPartArr.length === 1 ) {
            object.beginNodeText = object.barNum;
            object.endNodeText = object.barNum + 1;
            c_forces.push(0);
            c_forces.push(0);
        } else {
            object.endNodeText = object.barNum + 1;
            c_forces.push(0);
        }

        console.log(barPartArr);
        constrChanged = true;
    }
    else if ( barPartComponent.status == QQ.Component.Error ) {
        console.log("error creating component");
        console.log(barPartComponent.errorString());
    }
}


function addPart(parent, u_l, u_a, u_e, u_x, u_width, num)
{
    if ( barPartComponent != null ) { // component has been previously loaded
        createBarObject(parent, u_l, u_a, u_e, u_x, u_width, num);
        console.log("loaded")
        return;
    }

    barPartComponent = Qt.createComponent("BarPartBase.qml");
    if ( barPartComponent.status == QQ.Component.Loading ) { //Depending on the content, it can be ready or error immediately
        console.log("signal")
        barPartComponent.statusChanged.connect(createBarObject); //что за компонент и почемуy не barPartComponent?
    }
    else {
        console.log("fast")
        createBarObject(parent, u_l, u_a, u_e, u_x, u_width, num);
    }
}


function addRestrict(node)
{
    if ( node === 0 ) {
        var vBar = barPartArr[node];
        vBar.beginRestrWidth = vBar.width / 4;
        vBar.beginRestrHeight = vBar.height + 50;
        vBar.beginRestrVisible = true;
    }
    else if ( node === barPartArr.length ){
        var vBar = barPartArr[node-1];
        vBar.endRestrWidth = vBar.width / 4;
        vBar.endRestrHeight = vBar.height + 50;
        vBar.endRestrVisible = true;
    }
    else if ( node < barPartArr.length ) {
        var vBar = barPartArr[node];
        vBar.beginRestrVisible = true;
    }
    else {
        console.log("ERRORE");
    }

    constrChanged = true;
}


function removePart(parent, u_l, u_a, u_x)
{
    barPartArr.shift().destroy();
    addPart(parent, u_l, u_a, u_x, 10);
    return barPartArr.length;
}


function removeBar()
{
    barPartArr.pop().destroy();
    c_forces.pop();
    console.log(barPartArr);
    constrChanged = true;
}


function removeRestr(node)
{
    if ( node === 0 ) {
        var vBar = barPartArr[node];
        vBar.beginRestrVisible = false;
    }
    else if ( node === barPartArr.length ){
        var vBar = barPartArr[node-1];
        vBar.endRestrVisible = false;
    }
    else if ( node < barPartArr.length ) {
        var vBar = barPartArr[node];
        vBar.beginRestrVisible = false;
    }
    else {
        console.log("ERRORE");
    }

    constrChanged = true;
}


/* to display not so big bars */
function paramCoef(param)
{
    if ( param !== 1 )
        return param / 5 + 0.8;
    else
        return param;
}


function enumBars()
{
    var barCount = 0;
    for ( var i = 0; i < barPartArr.length; ++i ) {
            barPartArr[i].barNum = barCount;
            ++barCount;
    }
}


function show()
{
    for ( var i = 0; i < barPartArr.length; ++i ) {
        console.log(barPartArr[i].d_forceCoef, "--");
        console.log(c_forces[i]);
    }
    console.log(c_forces[barPartArr.length]);
    console.log("---------");
}


function showDistrForces(what)
{
    for ( var i = 0; i < barPartArr.length; ++i )
        if ( barPartArr[i].d_forceCoef != 0 )
            barPartArr[i].distrVisible = what;
}


function showConctrForces(what)
{
    for ( var i = 0; i < c_forces.length; ++i )
        if ( c_forces[i] != 0 )
            if ( i == 0 )
                barPartArr[i].beginConctrVisible = what;
            else if ( i == c_forces.length-1 )
                barPartArr[i-1].endConctrVisible = what;
            else {
                if ( c_forces[i] > 0 )
                    barPartArr[i].beginConctrVisible = what;
                else
                    barPartArr[i-1].endConctrVisible = what;
            }
}


function showNodes(what)
{
    for ( var i = 0; i < barPartArr.length; ++i ) {
        if ( i == 0 )
            barPartArr[i].beginNodeVisible = what;
        barPartArr[i].endNodeVisible = what;
    }
}


function addDistrForce(u_bar, startPos, endPos, coef)
{
    var bar = barPartArr[u_bar - 1];
    if ( !bar )
        console.log("ERROR"); //print error

    var direction = true;
    if ( coef > 0 )
        direction = true;
    else
        direction = false;

    if ( startPos != 0 || endPos != 1 ) {
        if ( startPos == 0 ) {
            var width = bar.width;
            bar.width = endPos * bar.width;
            var temp = bar.coef_L;
            bar.coef_L = bar.coef_L * endPos;
            addPart(bar.parent, temp - bar.coef_L, bar.coef_A,
                    bar.coef_E, bar.x + bar.width, width - bar.width, u_bar);
            if ( !direction )
                bar.distrRot = 180;
            bar.distrVisible = true;
            bar.d_forceCoef = coef;
        }
        else if ( endPos == 1 ) {
            var width = bar.width;
            bar.width = startPos * bar.width;
            var temp = bar.coef_L;
            bar.coef_L = bar.coef_L * startPos;
            addPart(bar.parent, temp - bar.coef_L, bar.coef_A, bar.coef_E,
                    bar.x + bar.width, width - bar.width, u_bar);
            barPartArr[u_bar].distrVisible = true;
            if ( !direction )
                barPartArr[u_bar].distrRot = 180;
            barPartArr[u_bar].distrVisible = true;
            barPartArr[u_bar].d_forceCoef = coef;
        }
        else {
            var width = bar.width;
            bar.width = startPos * bar.width;
            var temp = bar.coef_L;
            bar.coef_L = bar.coef_L * startPos;
            addPart(bar.parent, temp * endPos - bar.coef_L, bar.coef_A,
                    bar.coef_E, bar.x + bar.width, width * endPos - bar.width,
                    u_bar);
            var tempBar = barPartArr[u_bar];
            tempBar.distrVisible = true;
            if ( !direction )
                tempBar.distrRot = 180;
            tempBar.distrVisible = true;
            tempBar.d_forceCoef = coef;
            addPart(bar.parent, temp - temp * endPos, bar.coef_A, temp.coef_E,
                    tempBar.x + tempBar.width, width - width * endPos,
                    u_bar + 1);
        }
    }
    else {
        if ( !direction )
            bar.distrRot = 180;
        bar.distrVisible = true;
        bar.d_forceCoef = coef;
    }

    constrChanged = true;
}


function removeDistrForce(u_bar)
{
    var bar = barPartArr[u_bar - 1];
    if ( !bar )
        console.log("ERROR"); //print error
    bar.distrVisible = false;
    bar.d_forceCoef = 0;

    constrChanged = true;
}


function addConctrForce(u_bar, pos, coef)
{
    var bar = barPartArr[u_bar - 1];
    if ( !bar )
        console.log("ERROR"); //print error

    var direction;
    if ( coef < 0 )
        direction = false;
    else
        direction = true;

    console.log(u_bar, "----", u_bar - 1, "---", pos, "---", coef, "---", direction)
    if ( pos == 0 ) {
        if ( direction )
            bar.beginConctrVisible = true;
        else {
            var prevBar = barPartArr[u_bar - 2];
            if ( prevBar )
                prevBar.endConctrVisible = true;
            else {
                bar.beginConctrRot = 180;
                bar.beginConctrVisible = true;
            }
        }
        c_forces[u_bar-1] = coef;
    }
    else if ( pos == 1 ) {
        if ( direction ) {
            var nextBar = barPartArr[u_bar];
            if ( nextBar )
                nextBar.beginConctrVisible = true;
            else {
                bar.endConctrRot = 360;
                bar.endConctrVisible = true;
            }
        }
        else
            bar.endConctrVisible = true;
        c_forces[u_bar] = coef;
    }
    else {
        var width = bar.width;
        bar.width = bar.width * pos;
        var temp = bar.coef_L;
        bar.coef_L = bar.coef_L * pos;
        addPart(bar.parent, temp - bar.coef_L, bar.coef_A, bar.coef_E,
                bar.x + bar.width, width - bar.width, u_bar);
        if ( direction )
            barPartArr[u_bar].beginConctrVisible = true;
        else
            bar.endConctrVisible = true;
        c_forces[u_bar] = coef;
    }

    constrChanged = true;
}


function removeConctrForce(u_bar, pos)
{
    console.log(u_bar, pos);

    var bar = barPartArr[u_bar-1];
    if ( !bar )
        console.log("ERROR"); //print error

    if ( pos == 0 ) {
        bar.beginConctrVisible = false;
        if ( u_bar - 2 >= 0 )
            barPartArr[u_bar-2].endConctrVisible = false;
        c_forces[u_bar-1] = 0;
    }
    if ( pos == 1 ) {
        bar.endConctrVisible = false;
        if ( u_bar <= barPartArr.length )
            barPartArr[u_bar].beginConctrVisible = false;
        c_forces[u_bar] = 0;
    }

    constrChanged = true;
}


function normalize()
{
    for ( var i = 0; i < barPartArr.length - 1; ) {
        if ( mayJoin(barPartArr[i], barPartArr[i+1]) ) {
            var temp = barPartArr.splice(i+1, 1).shift();
            barPartArr[i].coef_L += temp.coef_L;
            barPartArr[i].width = 100 * paramCoef(barPartArr[i].coef_L);
            temp.destroy();
        }
        else
            ++i;
    }
    for ( var j = 0; j < barPartArr.length - 1; ++j ) {
        var end = barPartArr[j].x + barPartArr[j].width;
        if ( end !== barPartArr[j+1].x  )
            barPartArr[j+1].x = end;
    }

    constrChanged = true;
}


function mayJoin(one, two)
{
    if ( one.coef_E !== two.coef_E )
        return false;
    if ( one.coef_A !== two.coef_A )
        return false;
    if ( one.d_forceCoef !== two.d_forceCoef )
        return false;
    if ( one.beginConctrVisible || one.endConctrVisible )
        return false;
    if ( two.beginConctrVisible || two.endConctrVisible )
        return false;
//    if ( one.endConctrVisible && two.endConctrVisible )
//        return false;
//    if ( one.endRestrVisible )
//        return false;

    return true;
}


function scale(value)
{
    if ( barPartArr.length != 0 ) {
        scaleValue = value;
        barPartArr[0].width = 100 * paramCoef(barPartArr[0].coef_L)
                * value;
        barPartArr[0].height = 50 * paramCoef(barPartArr[0].coef_A)
                * value;
        for ( var i = 1; i < barPartArr.length; ++i ) {
            barPartArr[i].width = 100 * paramCoef(barPartArr[i].coef_L)
                    * value;
            barPartArr[i].height = 50 * paramCoef(barPartArr[i].coef_A)
                    * value;
            barPartArr[i].x = barPartArr[i-1].x + barPartArr[i-1].width;
        }
    }

    //            children[0].width *= coef;
    //            children[0].height *= coef;
    //            for ( var i = 1; i < children.length; ++i ) {
    //                children[i].width *= coef;
    //                children[i].height *= coef;
    //                children[i].x = children[i-1].x + children[i-1].width;
    //            }
}


function loadConstruction(parent, info)
{
    if ( info.error !== "no errors" )
        return info.error;

    var size = info.vec_L.length;
    for ( var i = 0; i < size; ++i )
        addPart(parent, info.vec_L[i],
                info.vec_A[i], info.vec_E[i]);
    for ( var i = 0; i < size; ++i ) {
        if ( info.d_Forces[i] != 0 ) {
            barPartArr[i].d_forceCoef = info.d_Forces[i];
            barPartArr[i].distrVisible = true;
        }
        if ( info.vec_Restr[i] != 0 )
            addRestrict(i);
    }
    if ( info.vec_Restr[size] != 0 )
        addRestrict(size);

    for ( var i = 0; i < size; ++i ) {
        if ( info.c_Forces[i] != 0 )
            addConctrForce(i+1, 0, info.c_Forces[i]);
        if ( i == size -1 )
            if ( info.c_Forces[i+1] != 0 )
                addConctrForce(i+1, 1, info.c_Forces[i]);
    }

    return info.error;
}
