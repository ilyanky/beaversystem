import QtQuick 2.2
import QtQuick.Controls 1.2
//import QtQuick.Window 2.0
//import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "logic.js" as Logic

//идея для переработки: создать промежуточное представление для таких вещей как диалоги и интерфейсы,
//которые взаимодействуют с кнопками, в виде слоев

ApplicationWindow {
    id: window
    //visible: true
    width: 1290
    height: 770
    title: "BeaverDam pre-alpha"

    Component.onCompleted: visible = true;

    function startCalculations()
    {
        if ( Logic.constrChanged === true ) {
            if ( Logic.barPartArr.length == 0 )
                emptyConstrDialog.open();
            else {
                var results = some.startCalc(Logic.barPartArr, Logic.c_forces);
                var vec_Nx_1 = [];
                var vec_Nx_2 = [];
                var vec_gx = [];
                for ( var i = 0; i < Logic.barPartArr.length; ++i ) {
                    vec_Nx_1[i] = results[0][i];
                    vec_Nx_2[i] = results[1][i];
                    vec_gx[i] = results[2][i];
                }
                post.updateInfo(vec_Nx_1, vec_Nx_2, vec_gx);
                if ( post.visible )
                    post.visible = false;
                post.open();
                Logic.constrChanged = false;
            }
        }
        else
            notChagedDialog.open();
    }

    function showResults()
    {
        if ( post.visible )
            post.visible = false;
        if ( Logic.constrChanged )
            post.updateInfo();
        post.open();
    }

    menuBar: MenuBar {
        Menu {
            title: "File"
            MenuItem {
                text: "New construction..."
                onTriggered: myContent.visible = true;
            }

            MenuItem {
                text: "Open..."
                onTriggered: openDialog.open();
            }
            MenuSeparator {}
            MenuItem {
                text: "Save"
            }
            MenuItem {
                text: "Save as..."
                onTriggered: saveAsDialog.open();
            }
            MenuSeparator {}
            MenuItem {
                text: "Exit"
                onTriggered: Qt.quit();
            }
        }

        Menu {
            title: "Edit"

            MenuItem {
                text: "Scale"
                onTriggered: Logic.scale(2);
            }
        }

        Menu {
            title: "View"
            MenuItem {
                text: "ShowDebug"
                onTriggered: Logic.show();
            }
            MenuSeparator {}
            MenuItem {
                text: "Show nodes"
                checkable: true
                onCheckedChanged: Logic.showNodes(checked);
            }
            MenuItem {
                text: "Show distrtibuted forces"
                checkable: true
                checked: true
                onCheckedChanged: Logic.showDistrForces(checked);
            }
            MenuItem {
                text: "Show concentrated forces"
                checkable: true
                checked: true
                onCheckedChanged: Logic.showConctrForces(checked);
            }

            CalcResultDialog {
                id: post
            }
        }

        Menu {
            title: "About"
            MenuItem {
                text: "About BeaverDam..."
                onTriggered: aboutWindow.open();
            }
        }
    }

    //toolBar: ToolBar {}

    //        RowLayout {
    //            anchors.fill: parent
    //            ToolButton {
    //                iconSource: "resources/play30.png";
    //                onClicked: ;//some.startCalc(Logic.barPartArr, Logic.c_forces);//передать данные и запустить код на сях
    //            }
    //        }

    CentralWidget {
        id: myContent
        anchors.fill: parent
        visible: false
    }

    AboutWindow {
        id: aboutWindow
    }

    FileDialog {
        id: openDialog
        title: "Open file"
        selectFolder: false
        selectMultiple: false
        nameFilters: [ "BeaverDamProject files (*.bdp)", "All files (*)" ]
        onAccepted: {
            var result = some.open(fileUrl);
            console.log(result);
            var error = Logic.loadConstruction(myContent.coordArrowX,
                                               result);
            if ( error === "no errors" )
                myContent.visible = true;
            else {
                errorConstrDialog.text += error;
                errorConstrDialog.open();
            }
        }
    }
    FileDialog {
        id: saveAsDialog
        title: "Save as"
        selectFolder: false
        selectMultiple: false
        selectExisting: false
        nameFilters: [ "BeaverDamProject files (*.bdp)", "All files (*)" ]
        onAccepted: some.saveAs(fileUrl, Logic.barPartArr, Logic.c_forces);
    }
    MessageDialog {
        id: notChagedDialog
        icon: StandardIcon.Warning
        text: "Сonstruction is not changed and result will not be chaged too. Show results again?"
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: { if ( post.visible )
                   post.visible = false;
               post.open();
        }
        onNo: close();
    }
    MessageDialog {
        id: emptyConstrDialog
        icon: StandardIcon.Warning
        text: "Сonstruction is empty, nothing to calculate!"
        standardButtons: StandardButton.Close
    }
    MessageDialog {
        id: errorConstrDialog
        icon: StandardIcon.Critical
        text: "Error: "
        standardButtons: StandardButton.Close
    }
}
