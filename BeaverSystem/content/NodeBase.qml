import QtQuick 2.0

Rectangle {
    property bool restrict
    property int startBar
    property int endBar
    property int startPos
    property int endPos
    property alias text: txt.text

    width: 20
    height: 20
    color: "transparent"
    border.color: "black"
    border.width: 1

    children:

    Text {
        id: txt
        anchors.fill: parent
        text: "10"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
