import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import "logic.js" as Logic


ToolBar {
    visible: false
    RowLayout {
        spacing: 10
        Image {
            source: "resources/bigRestr30.svg"
            sourceSize: Qt.size(30, 30)
        }
        Text {
            text: "Set a node:"
        }
        TextField {
            id: setNode
            text: "0"
            inputMask: "D9"
            implicitWidth: 50
        }

        Button {
            text: "ADD"
            onClicked: Logic.addRestrict(parseInt(setNode.text));
        }
        Button {
            text: "REMOVE"
            onClicked: Logic.removeRestr(parseInt(setNode.text));
        }
    }
}
