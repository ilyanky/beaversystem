import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2

Dialog {
    id: dialog

//    function changeText()
//    {
//        var num = parseInt(txtField.text);
//        if ( num == 0 )
//            num = 1;
//        var str;
//        if ( param == "E" ) {
//            str = num;
//            str += obj.text.substring(obj.text.indexOf("E"));
//            obj.text = str;
//        }
//        else if ( param == "A" ){
//            str = obj.text.substring(0, obj.text.indexOf("E") + 1);
//            str = str + ", " + num + "A";
//        }
//        else if ( param == "Lenght" ) {
//            str = num + "L";
//        }
//    }

    property alias text: txt.text
    readonly property alias newValue: txtField.text
    property alias mask: txtField.inputMask
//    signal txtFieldChanged()
//    property var obj
//    property string param

    width: 350
    height: 40
    modality: Qt.ApplicationModal
//    standardButtons: StandardButton.Ok | StandardButton.Cancel

    /*contentItem:*/ Row {
        //anchors.fill: parent
        spacing: 5
        Text {
            id: txt
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 12
        }
        TextField {
            id: txtField
            anchors.verticalCenter: parent.verticalCenter
            width: 50
            inputMask: "D9"
            text: "0"
        }
        Button {
            anchors.verticalCenter: parent.verticalCenter
            width: 50
            text: "OK"
            onClicked: {
//                changeText();
//                dialog.txtFieldChanged();
//                dialog.close();
                dialog.accept();
            }
        }
    }
}
