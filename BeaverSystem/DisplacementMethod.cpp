#include "DisplacementMethod.h"
#include <QCoreApplication>
#include <QDebug>
#include <cmath>

double* getMatrix_A(const double* const vec_L, const double* const vec_E,
                   const double* const vec_A, const double* vec_restr,
                   const int& len)
{
    const int K = 2;
    const int M = len + 1;

    double matrix_K[len][K][K];
    for ( int n = 0; n < len; ++n )
        for ( int i = 0; i < K; ++i )
            for ( int j = 0; j < K; ++j )
                if ( i == j )
                    matrix_K[n][i][j] = vec_E[n] * vec_A[n] / vec_L[n];
                else
                    matrix_K[n][i][j] = -(vec_E[n] * vec_A[n] / vec_L[n]);


    double* matrix_A = new double[M * M];
    for ( int i = 0; i < M * M; ++i )
        matrix_A[i] = 0;

    for ( int n = 0; n < len; ++n )
        for ( int i = n; i <= n + 1; ++i )
            for ( int j = n; j <= n + 1; ++j )
                if ( i == j )
                    *(matrix_A + i * M + j) = *(matrix_A + i * M + j) +
                        matrix_K[n][i - n][j - n];
                else
                    *(matrix_A + i * M + j) = matrix_K[n][i - n][j - n];

    for ( int i = 0; i < M; ++i )
        if ( vec_restr[i] )
            for ( int j = 0; j < M; ++j )
                if ( i == j )
                    *(matrix_A + i * M + j) = 1;
                else
                    *(matrix_A + i * M + j) = 0;

    return matrix_A;
}


double* getVector_b(const double* const d_forces, const double* const c_forces,
                   const double* const vecL, const double* restr, const int& len)
{
    double* vec_b = new double[len];
    for ( int i = 0; i < len; ++i )
        vec_b[i] = 0;

    double vec_q[len - 1];
    for ( int i = 0; i < len - 1; ++i )
        vec_q[i] = -(d_forces[i] * vecL[i] / 2);

    for ( int i = 0; i < len; ++i ) {
        if ( i == 0 )
            vec_b[i] = c_forces[i] - vec_q[i];
        else if ( i == len - 1 )
            vec_b[i] = c_forces[i] - vec_q[i - 1];
        else
            vec_b[i] = c_forces[i] - vec_q[i - 1] - vec_q[i];
    }

    for ( int i = 0; i < len; ++i )
        if ( restr[i] == 1 )
            vec_b[i] = 0;

    return vec_b;
}

double* getDeltas(const double* const matrix_A, const double* const vec_b,
                 const int& len)
{
    double a[len][len];
    double b[len];

    for ( int i = 0; i < len; ++i )
        for ( int j = 0; j < len; ++j )
            a[i][j] = *(matrix_A + i * len + j);
    for ( int i = 0; i < len; ++i )
        b[i] = vec_b[i];

    double* x = new double[len];


    /* Gauss method */
    double v = 0;
    for ( int k = 0; k < len - 1; ++k )
    {
        int im = k;
        for( int i = k + 1; i < len; ++i )
            if( std::abs(a[im][k]) < std::abs(a[i][k]) )
                im = i;
        if ( im != k ) {
            for ( int j = 0; j < len; ++j ) {
                v = a[im][j];
                a[im][j] = a[k][j];
                a[k][j] = v;
            }
            v = b[im];
            b[im] = b[k];
            b[k] = v;
        }
        for ( int i = k + 1; i < len; ++i ) {
            v = 1.0 * a[i][k] / a[k][k];
            a[i][k] = 0;
            b[i] = b[i] - v * b[k];
            if( v != 0 )
                for ( int j = k + 1; j < len; ++j )
                    a[i][j] = a[i][j] - v * a[k][j];
        }
    }

    double s = 0;
    x[len-1] = 1.0 * b[len-1] / a[len-1][len-1];
    for ( int i = len - 2; i >= 0; --i ) {
        s = 0;
        for( int j = i + 1; j < len; ++j )
            s = s + a[i][j] * x[j];
        x[i] = 1.0 * (b[i] - s) / a[i][i];
    }


    const double SMALL = 0.000001;
    for ( int i = 0; i < len; ++i )
        if ( std::abs(x[i]) < SMALL ) //возможна ошибка
            x[i] = 0;

    return x;
}


QList<QList<double> > getStrengthCalcs(const std::pair<int, QList<double*> > info)
{
    QList<QList<double> > result;

    const int BARS = info.first;
    const int NODES = BARS + 1;

    double* vecL = info.second.at(0);
    double* vecE = info.second.at(1);
    double* vecA = info.second.at(2);
    double* d_forces = info.second.at(3);
    double* vec_restr = info.second.at(4);
    double* c_forces = info.second.at(5);

    double* A = getMatrix_A(vecL, vecE, vecA, vec_restr, BARS);
    qDebug() << "------";
    for ( int i = 0; i < NODES * NODES; ++i )
        qDebug() << A[i];

    double* vec_b = getVector_b(d_forces, c_forces, vecL, vec_restr, NODES);
    qDebug() << "------";
    for ( int i = 0; i < NODES; ++i )
        qDebug() << vec_b[i];

    double* deltas = getDeltas(A, vec_b, NODES);
    qDebug() << "------";
    for ( int i = 0; i < NODES; ++i )
        qDebug() << deltas[i];

    const int J = 2;
    double vec_Nx[BARS][J];

    for ( int i = 0; i < BARS; ++i ) {
        vec_Nx[i][0] = vecE[i] * vecA[i] / vecL[i] *
                (deltas[i+1] - deltas[i]) + d_forces[i] * vecL[i] / 2;
        if ( d_forces[i] != 0 )
            vec_Nx[i][1] = -(d_forces[i] * vecL[i] / 2 * (2 / vecL[i]));
        else
            vec_Nx[i][1] = 0;
    }

    QList<double> res_vec;
    for ( int i = 0; i < BARS; ++i )
        res_vec.push_back(vec_Nx[i][0]);
    result.push_back(res_vec);
    res_vec.clear();
    for ( int i = 0; i < BARS; ++i )
        res_vec.push_back(vec_Nx[i][1]);
    result.push_back(res_vec);
    res_vec.clear();

    for ( int i = 0; i < BARS; ++i )
            vec_Nx[i][1] = vec_Nx[i][0] + vec_Nx[i][1] * vecL[i];

    qDebug() << "------";
    for ( int i = 0; i < BARS; ++i )
        for ( int j = 0; j < J; ++j )
            qDebug() << std::abs(vec_Nx[i][j]);

    double vec_qx[BARS];

//    int max_i = 0;
//    float max = std::abs(vec_Nx[0][0]);
    for ( int i = 0; i < BARS; ++i ) {
        double one = std::abs(vec_Nx[i][0]);
        double two = std::abs(vec_Nx[i][1]);
        if ( one < two ) {
//            if ( max < two ) {
//                max = two;
//                max_i = i;
//            }
            vec_qx[i] = vec_Nx[i][1] / vecA[i];
        }
        else {
//            if ( max < one ) {
//                max = one;
//                max_i = i;
//            }
            vec_qx[i] = vec_Nx[i][0] / vecA[i];
        }
    }
    qDebug() << "------";
//    qDebug() << max_i << max;
    for ( int i = 0; i < BARS; ++i )
        res_vec.push_back(vec_qx[i]);
    result.push_back(res_vec);

//    if ( std::abs(vec_Nx[max_i][0]) == max )
//        max = vec_Nx[max_i][0];
//    else
//        max = vec_Nx[max_i][1];

//    float gx = max / vecA[max_i];
//    qDebug() << "------";
//    qDebug() << gx;

//    delete[] vecL;
//    delete[] vecE;
//    delete[] vecA;
//    delete[] d_forces;
//    delete[] c_forces;
//    delete[] vec_restr;

    delete[] A;
    delete[] vec_b;
    delete[] deltas;

    return result;
}


//std::pair<int, QList<float*> > getInfo(const QVariant& barPartArr, const QVariant& c_forces)
//{

//}
