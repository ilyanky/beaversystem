#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <BeaverSystem.h>

int main(int argc, char *argv[])
{
    qDebug() << argc;

    //QGuiApplication::setDesktopSettingsAware();
    QApplication app(argc, argv);

    BeaverSystem sys;
    sys.load();

    return app.exec();
}
