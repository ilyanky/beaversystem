#ifndef BEAVERSYSTEM_H
#define BEAVERSYSTEM_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQuickView>
#include <QQmlContext>
#include <QJSValue>

class BeaverSystem : public QObject
{
    Q_OBJECT

private:
    const int MAGIC = 0x17FF34;
    QQmlApplicationEngine m_engine;

public:
    explicit BeaverSystem(QObject* parent = 0);
    void load();
    Q_INVOKABLE void foo() { qDebug() << "this is a test"; }
    Q_INVOKABLE QJSValue startCalc(const QJSValue& barArr, const QJSValue& forces);
    Q_INVOKABLE bool saveAs(const QUrl& url, const QJSValue& barArr,
                            const QJSValue& forces);
    Q_INVOKABLE QJSValue open(const QUrl& url);

signals:

public slots:

};

#endif // BEAVERSYSTEM_H
