import QtQuick 2.0

Rectangle {
    property real forceCoef

    property alias source: img.source
    property alias scale: img.scale
    property alias fillMode: img.fillMode

    width: 100
    height: 50
    radius: 5
    border.width: 0
    color: "transparent"

    Image {
        id: img
        anchors.fill: parent
        scale: 0.9
        opacity: 0.7
        smooth: true
        antialiasing: true
    }
}
