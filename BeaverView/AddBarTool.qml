import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import "logic.js" as Logic

ToolBar {
    visible: false
//    height: 33
    RowLayout {
        spacing: 10
        Image {
            source: "resources/rect.png"
            sourceSize: Qt.size(30, 30)
        }
        Text {
            text: "Set a L(coef):"
        }
        TextField {
            id: setL
            text: "1"
            inputMask: "D9"
            implicitWidth: 50
        }

        Text {
            text: "Set a A(coef):"
        }
        TextField {
            id: setA
            text: "1"
            inputMask: "D9"
            implicitWidth: 50
        }

        Text {
            text: "Set a E(coef):"
        }
        TextField {
            id: setE
            text: "1"
            inputMask: "DD.99"
            implicitWidth: 50
        }

        Button {
            text: "ADD"
            onClicked: {
                var l = parseInt(setL.text);
                var a = parseInt(setA.text);
                var e = parseFloat(setE.text);
                if ( l === 0 )
                    l = 1;
                if ( a === 0 )
                    a = 1;
                Logic.addPart(myContent.coordArrowX, l, a, e);
//                Logic.barPartArr[Logic.barPartArr.length-1].coef_E =
//                        parseFloat(setE.text);
            }
        }
        ToolButton {
            implicitHeight: setE.height
            iconSource: "resources/remove.svg"
            tooltip: "Delete last part"
            onClicked: Logic.removeBar();
        }
//        ToolButton {
//            iconSource: "resources/play30.png";
//            onClicked: ;//some.startCalc(Logic.barPartArr, Logic.c_forces);//передать данные и запустить код на сях
//        }
    }
}
