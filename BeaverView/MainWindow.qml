import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "logic.js" as Logic
import "Construction.js" as Construction

//ApplicationWindow {
//    id: window
//    visible: true
//    width: 1290
//    height: 770
//    title: "BeaverDam pre-alpaha"

//    Window {

//    }

//    menuBar: MenuBar {
//        Menu {
//            title: "File"
//            MenuItem {
//                text: "Save as..."
//                onTriggered: {
//                    saveAsDialog.open();
//                }
//            }
//            FileDialog {
//                id: saveAsDialog
//                title: "Save as"
//                selectFolder: false
//                selectMultiple: false
//                selectExisting: false
//                nameFilters: [ "BeaverDamProject files (*.bdp)", "All files (*)" ]
//                onAccepted: console.log(saveAsDialog.fileUrl)
//            }
//        }

//        Menu {
//            title: "Edit"
//            MenuItem {
//                text: "post"
//                onTriggered: {
//                    var content = Qt.createComponent("TabContent.qml");
//                    for ( var i = 0; i < Logic.barPartArr.length; ++i ) {
//                        post.tabView.addTab(i, content);
//                    }
//                    post.open();
//                }
//            }
//            MenuItem {
//                text: "Scale"
//                onTriggered: Logic.scale(2);
//            }

//            CalcResultDialog {
//                id: post
////                constrInfo: Logic.setInfo();
//            }
//        }

//        Menu {
//            title: "View"
//            MenuItem {
//                text: "Show nodes"
//                checkable: true
//                onCheckedChanged: Logic.showNodes(checked);
//            }
//            MenuItem {
//                text: "Show distrtibuted forces"
//                checkable: true
//                checked: true
//                onCheckedChanged: Logic.showDistrForces(checked);
//            }
//            MenuItem {
//                text: "Show concentrated forces"
//                checkable: true
//                checked: true
//                onCheckedChanged: Logic.showConctrForces(checked);
//            }
//        }

//        Menu {
//            title: "About"
//            MenuItem {
//                text: "About BeaverDam..."
//                onTriggered: aboutWindow.open()
//            }
//        }
//    }

//    //toolBar: ToolBar {}

//    //        RowLayout {
//    //            anchors.fill: parent
//    //            ToolButton {
//    //                iconSource: "resources/play30.png";
//    //                onClicked: ;//some.startCalc(Logic.barPartArr, Logic.c_forces);//передать данные и запустить код на сях
//    //            }
//    //        }

//    CentralWidget {
//        id: myContent
//        anchors.fill: parent
//    }


//    AboutWindow {
//        id: aboutWindow
//    }
//}

Rectangle {
    width: 100
    height: 100

    focus: true
    Keys.onReturnPressed: Qt.quit();

    Component.onCompleted: {
        var constr = Construction.makeConstruction();
        constr.addBar(10);
        constr.addBar(10);
        constr.addBar(10);
        constr.show();
        constr.insertBar(0, 50);
        constr.insertBar(2, 50);
        constr.removeBar(0);
        constr.removeBar(1);
        constr.show();
    }
}
