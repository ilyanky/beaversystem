import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import "logic.js" as Logic

ToolBar {
    visible: false
    RowLayout {
        spacing: 10
        Image {
            source: "resources/distrForce30.svg"
            sourceSize: Qt.size(30, 30)
        }

        Text {
            text: "Set a bar:"
        }
        TextField {
            id: setForceBarField
            text: "1"
            inputMask: "D9"
            implicitWidth: 50
        }

        Text {
            text: "Set a start pos(coef):"
        }
        TextField {
            id: setPosCoefField
            text: "0"
            inputMask: "9.D"
            implicitWidth: 50
        }

        Text {
            text: "Set force coef:"
        }
        TextField {
            id: setForceCoefField
            text: "1"
            inputMask: "#DD.DDD"
            implicitWidth: 50

            PropertyAnimation {
                id: anim
                target: setForceCoefField
                property: "textColor"
                from: "red"
                to: "black"
                loops: 5
            }

//            onTextChanged: {
//                if ( text.)
//            }
        }
        ComboBox {
            model: [ "qL", "F" ]
            implicitWidth: 50
        }

        Button {
            text: "ADD"
            onClicked: {
                var direction;
                if ( setForceCoefField.text != 0 )
                    Logic.addConctrForce(parseInt(setForceBarField.text),
                                         parseFloat(setPosCoefField.text),
                                         parseFloat(setForceCoefField.text));
                else
                    anim.start();
            }
        }
        Button {
            text: "REMOVE"
            onClicked: Logic.removeConctrForce(parseInt(setForceBarField.text),
                                               parseInt(setPosCoefField.text));
        }
    }

}
