
//!------- 1) применять как можно больше стандартный принцип декомпозиции
//!------- 2) так как будет использоваться большое количество операции new
//            в данном замыкании, то нужно не забыть написать специальную
//            функцию зачищающую все это дело... Впрочем не обязательно
//            использовать new


// Main wrap function. Describes a Construction
function makeConstruction()
{
    var m_NodeArr = [];
    var m_BarArr = [];


    function createNode(ux) {
        var tempNode = { xPos: ux };
        return tempNode;
    }


    function createBar(unode1, unode2) {
        var tempBar = {
            beginNode: unode1,
            endNode: unode2,
            getDebugStr: function() {
                return "[ " + this.beginNode.xPos +
                        " " + this.endNode.xPos + " ] ";
            }
        }
        return tempBar;
    }


    function _addBar(ulength) {
        if ( ulength === undefined ) {
            console.log("ERROR! From _addBar: ulength is undefined");
            return;
        }
        else if ( ulength <= 0 ) {
            console.log("ERROR! From _addBar: ulength is incorrect");
            return;
        }

        if ( m_NodeArr.length === 0 )
        {
            m_NodeArr[0] = createNode(0);
            m_NodeArr[1] = createNode(ulength);
            m_BarArr[0] = createBar(m_NodeArr[0], m_NodeArr[1]);
            return m_BarArr[0];
        } else {
            var beginNode = m_NodeArr[m_NodeArr.length - 1];
            var endNode = createNode(beginNode.xPos + ulength);
            m_NodeArr.push(endNode);
            m_BarArr.push(createBar(beginNode, endNode));
            return m_BarArr[m_BarArr.length - 1];
        }
    }


    function _insertBar(unode, ulength) {
        if ( unode === undefined ) {
            console.log("ERROR! From _insertBar: unode is undefined");
            return;
        }
        else if ( unode < 0 ) {
            console.log("ERROR! From _insertBar: unode is incorrect");
            return;
        }
        else if ( ulength === undefined ) {
            console.log("ERROR! From _insertBar: ulength is undefined");
            return;
        }
        else if ( ulength <= 0 ) {
            console.log("ERROR! From _insertBar: ulength is incorrect");
            return;
        }
        else if ( unode >= m_NodeArr.length - 1 ) {
            _addBar(ulength);
            return;
        }
        else
        {
            var ux = m_NodeArr[unode].xPos + ulength;
            m_NodeArr.splice(unode + 1, 0, createNode(ux));
            for ( var i = unode + 2; i < m_NodeArr.length; ++i )
                m_NodeArr[i].xPos += ulength;

            m_BarArr[unode].beginNode = m_NodeArr[unode+1];
            m_BarArr.splice(unode, 0,
                            createBar(m_NodeArr[unode], m_NodeArr[unode+1]));
        }
    }


    function _popBar() {
        if ( m_BarArr.length === 1 )
            m_NodeArr.pop();

        m_NodeArr.pop();
        m_BarArr.pop();
    }


    function _removeBar(unum) {
        if ( unum === undefined ) {
            console.log("ERROR! From _removeBar: unum is undefined");
            return;
        }
        else if ( unum >= m_BarArr.length || unum < 0 ) {
            console.log("ERROR! From _removeBar: unum is incorrect");
            return;
        }
        else
        {
            for ( var i = unum; i < m_BarArr.length - 1; ++i )
                m_NodeArr[i+1].xPos = m_NodeArr[i].xPos +
                        m_NodeArr[i+2].xPos - m_NodeArr[i+1].xPos;
            _popBar();
        }
    }


    function debug() {
        var str = "Nodes[ ";
        for ( var i = 0; i < m_NodeArr.length; ++i )
            str += (m_NodeArr[i].xPos + " ");
        console.log( str += "]" );

        str = "Bars[ ";
        for ( var i = 0; i < m_BarArr.length; ++i )
            str += m_BarArr[i].getDebugStr();
        console.log( str += "]" );
    }


    return {
        addBar: _addBar,
        insertBar: _insertBar,
        popBar: _popBar,
        removeBar: _removeBar,
        show: debug
    }
}
