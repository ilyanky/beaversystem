import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2
import "logic.js" as Logic

Item {

    //убрать позже эту оболочку
    id: base

//    function currentInfo() {
//        var str = "E(coef): ";
//        str += setECoefDialog.textField + "\n";
//        str += "A(coef): ";
//        str += coef_A + "\n";
//        str += "L(coef): ";
//        str += coef_L + "\n";
//        str += "Length: ";
//        str += setRealLengthDialog.textField + "\n";
//        str += "Real value of E parametr: "
//        str += setRealEDialog.textField + "\n";
//        str += "Real value of A parametr: ";
//        str += setRealADialog.textField;
//        return str;
//    }

    property real coef_L: 0
//    property real prevCoef_L: 0
    property real coef_A: 0
    property real coef_E: 0
    property real d_forceCoef: 0
//    property real c_forceCoef: 0
    property int barNum: 0

    property string borderColor: "#a90dc8"
    property int borderWidth: 3

    property alias color: rect.color
    property alias eaText: eaTxt.text
    property alias lText: lTxt.text
    property alias lTextVisible: lTxt.visible
    property alias numTextVisible: numTxt.visible
    property alias acceptedButtons: mouseArea.acceptedButtons

    property alias beginNodeVisible: beginNode.visible
    property alias endNodeVisible: endNode.visible
    property alias beginNodeText: beginNode.text
    property alias endNodeText: endNode.text

    property alias beginConctrVisible: beginConctrForce.visible
    property alias endConctrVisible: endConctrForce.visible
    property alias distrVisible: distrForce.visible
    property alias beginConctrRot: beginConctrForce.rotation
    property alias endConctrRot: endConctrForce.rotation

    property alias beginRestrVisible: beginRestriction.visible
    property alias endRestrVisible: endRestriction.visible
    property alias beginRestrWidth: beginRestriction.width
    property alias beginRestrHeight: beginRestriction.height
    property alias endRestrWidth: endRestriction.width
    property alias endRestrHeight: endRestriction.height

    width: 100
    height: 50
    z: 1

//    transformOrigin: Item.Left

    onCoef_LChanged: {
        lTxt.text = coef_L + "L";
    }

    onCoef_EChanged: {
        eaTxt.text = coef_E + "E, " + coef_A + "A";
    }

    Rectangle {
        id: rect
        color: "white"
        anchors.fill: parent
        border.color: parent.borderColor
        border.width: parent.borderWidth
        opacity: 0.9
        radius: 5

        Text {
            id: eaTxt
            anchors.centerIn: parent
            text: "E, A"
            font.bold: true
            font.pointSize: 10
            opacity: mouseArea.containsMouse ? parent.opacity : 0
        }

        Menu {
            id: menu

            MenuItem {
                text: "Info..."
                onTriggered: msgDialog.open();
            }
            MenuSeparator { }
            MenuItem {
                text: "Set E(coef)..."
                onTriggered: setECoefDialog.open();
            }
            MenuItem {
                text: "Set real E..."
                onTriggered: setRealEDialog.open();
            }
            MenuItem {
                text: "Set L(coef)..."
                onTriggered: setLDialog.open();
            }
            MenuItem {
                text: "Set real A..."
                onTriggered: setRealADialog.open();
            }


//            MessageDialog {
//                id: msgDialog
//                text: currentInfo();
//            }

//            MiniDialog {
//                id: setECoefDialog
//                text: "Set coefficient of E parametr: "
////                obj: eaTxt
////                param: "E"
////                onTxtFieldChanged: base.coef_E = textField
//            }
//            MiniDialog {
//                id: setRealEDialog
//                text: "Set value of E parametr: "
//                //obj: eaTxt

//            }
            MiniDialog {
                id: setLDialog
                text: "Set value of L(coef) parametr: "
                onAccepted: coef_L = parseFloat(newValue);
                //onC
//                obj: lTxt
//                param: "Lenght"
            }
//            MiniDialog {
//                id: setRealADialog
//                text: "Set value of A parametr: "
////                obj: eaTxt
////                param: "A"
//            }
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: menu.popup()
        }
    }

    Text {
        id: numTxt
        anchors.horizontalCenter: rect.horizontalCenter
        anchors.bottom: rect.top
        font.bold: true
        font.pointSize: 10
        opacity: mouseArea.containsMouse ? rect.opacity : 0
        text: barNum + 1
    }
    Text {
        id: lTxt
        anchors.horizontalCenter: rect.horizontalCenter
        anchors.top: rect.bottom
        font.bold: true
        font.pointSize: 10
        opacity: mouseArea.containsMouse ? rect.opacity : 0
        text: "L"
    }

    NodeBase {
        id: beginNode
        visible: false
        anchors.bottom: parent.top
        anchors.left: parent.left
    }
    NodeBase {
        id: endNode
        visible: false
        anchors.bottom: parent.top
        anchors.right: parent.right
    }

    ForceBase {
        id: beginConctrForce
        source: "./resources/concentratedForce.svg"
        //scale: 1
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width / 2
        visible: false
    }
    ForceBase {
        id: endConctrForce
        source: "./resources/concentratedForce.svg"
        rotation: 180
        //scale: 1
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width / 2
        visible: false
    }
    ForceBase {
        id: distrForce
        source: "./resources/distrForce2.svg"
        scale: 1
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        visible: false
    }

    RestrictBase {
        id: beginRestriction
        source: "./resources/restr90.svg"
        width: 25
        height: 34
        anchors.right: parent.left
        anchors.verticalCenter: parent.verticalCenter
        scale: 1
        visible: false
    }
    RestrictBase {
        id: endRestriction
        source: "./resources/restr270.svg"
        width: 25
        height: 34
        anchors.left: parent.right
        anchors.verticalCenter: parent.verticalCenter
        scale: 1
        visible: false
    }
}

