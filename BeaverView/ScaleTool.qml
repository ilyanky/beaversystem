import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import "logic.js" as Logic

ToolBar {
    visible: false
    RowLayout {
        spacing: 10
        Image {
            source: "resources/magnifier.png"
            sourceSize: Qt.size(30, 23)
        }

        Text {
            text: "Choose value:"
        }
        ComboBox {
            id: comboBox
            model: [ "10%", "30%", "50%", "100%", "120%",
                "150%", "200%", "300%" ]
            currentIndex: 3
            implicitWidth: 70

            onCurrentTextChanged: {
                console.log(currentText);
                Logic.scale(parseInt(currentText.slice(0, currentText.length-1))
                            / 100);
            }
        }

//        Button {
//            text: "ADD"
//            onClicked: {
//                var direction;
//                if ( setForceCoefField.text != 0 )
//                    Logic.addConctrForce(parseInt(setForceBarField.text),
//                                         parseFloat(setPosCoefField.text),
//                                         parseFloat(setForceCoefField.text));
//                else
//                    anim.start();
//            }
//        }
//        Button {
//            text: "REMOVE"
//            onClicked: Logic.removeConctrForce(parseInt(setForceBarField.text),
//                                               parseInt(setPosCoefField.text));
//        }
    }

}
