import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import "logic.js" as Logic

ToolBar {
    visible: false
    RowLayout {
        spacing: 10
        Image {
            source: "resources/distrForce30.svg"
            sourceSize: Qt.size(30, 30)
        }
        Text {
            text: "Set a bar:"
        }
        TextField {
            id: setBarField
            text: "1"
            inputMask: "DD.99"
            implicitWidth: 50
        }

        Text {
            text: "Set a begin(coef):"
        }
        TextField {
            id: setBeginCoefField
            text: "0"
            inputMask: "9.9"
            implicitWidth: 50
        }

        Text {
            text: "Set a end(coef):"
        }
        TextField {
            id: setEndCoefField
            text: "0.5"
            inputMask: "9.9"
            implicitWidth: 50
        }

        Text {
            text: "Set force coef:"
        }
        TextField {
            id: setDistrForceCoefField
            text: "1"
            inputMask: "#DD.DDD"
            implicitWidth: 50

            PropertyAnimation {
                id: anim
                target: setDistrForceCoefField
                property: "textColor"
                from: "red"
                to: "black"
                loops: 5
            }
        }
        ComboBox {
            model: [ "q", "F" ]
            implicitWidth: 30
        }

        Button {
            text: "ADD"

            onClicked: {
                var direction;
                if ( setDistrForceCoefField.text != 0 ) {
                    if ( setDistrForceCoefField.text > 0 )
                        direction = true;
                    else
                        direction = false;
                    Logic.addDistrForce(parseInt(setBarField.text),
                                        parseFloat(setBeginCoefField.text),
                                        parseFloat(setEndCoefField.text), direction,
                                        parseFloat(setDistrForceCoefField.text));
                }
                else
                    anim.start();
            }
        }
        Button {
            text: "REMOVE"
            onClicked: Logic.removeDistrForce(parseInt(setBarField.text));
        }

    }

}
